package com.example.myattachment.network


import com.example.myattachment.models.auth.SignIn
import com.example.myattachment.models.auth.SignUp
import com.example.myattachment.models.auth.SignInRes
import retrofit2.Call
import retrofit2.http.*

interface EndpointApis {
    @POST("auth/sign-up")
    fun signUp (@Body signUp: SignUp?) : Call<SignInRes>

    @POST("auth/sign-in")
    fun signIn (@Body signIn: SignIn?) : Call<SignInRes>
}