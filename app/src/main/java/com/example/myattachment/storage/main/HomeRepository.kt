package com.example.myattachment.storage.main

import android.app.Application
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.myattachment.models.user.User
import com.example.myattachment.models.generic.Resource

class HomeRepository (application: Application) {
    val updateFirebaseTokenObservable = MutableLiveData<Resource<User>>()
    private val context: Context = application.applicationContext
}