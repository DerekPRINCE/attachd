package com.example.myattachment.storage.data

import android.content.Context
import android.content.SharedPreferences
import com.example.myattachment.models.address.Address
import com.example.myattachment.models.user.User
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class PrefrenceManager(internal var _context: Context) {
    internal var pref: SharedPreferences
    internal var editor: SharedPreferences.Editor
    internal var PRIVATE_MODE = 0

    companion object {
        private val LOGIN_STATUS = "LOGIN_STATUS"
        private val PROFILE = "events_user_profile"
        private val FIREBASE_TOKEN = "firebasetoken"
        private val PREF_NAME = "events_prefrences"
        private val ADDRESS = "att_address"
        private val USER_TYPE = "user_type"
    }

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun clearUser() {
        editor.clear()
        editor.commit()
    }

    fun setLoginStatus(status: Int){
        editor.putInt(LOGIN_STATUS,status)
        editor.commit()
    }

    fun setUserType(type: Int){
        editor.putInt(USER_TYPE,type)
        editor.commit()
    }

    fun getUserType() : Int?{
        return pref.getInt(USER_TYPE,0)

    }

    fun setAddress(address: Address?){
        editor.putString(ADDRESS, Gson().toJson(address))
        editor.commit()
    }


    fun getAddress() : Address?{
        val listType = object : TypeToken<Address>() {}.type
        return Gson().fromJson<Address>(pref.getString(ADDRESS, ""), listType)
    }

    fun setFirebaseToken(token: String?){
        editor.putString(FIREBASE_TOKEN, token)
        editor.commit()
    }

    fun getFirebaseToken() : String?{
        return pref.getString(FIREBASE_TOKEN, "")
    }

    fun setUserSession(user: User?){
        editor.putString(PROFILE, Gson().toJson(user))
        editor.commit()
    }

    fun getUserSession () : User?{
        val listType = object : TypeToken<User>() {}.type
        return Gson().fromJson<User>(pref.getString(PROFILE, ""), listType)
    }

    fun getLoginStatus(): Int {
        return pref.getInt(LOGIN_STATUS,0)
    }
}