package com.example.myattachment.storage.auth

import android.app.Application
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myattachment.R
import com.example.myattachment.models.auth.SignIn
import com.example.myattachment.models.auth.SignInRes
import com.example.myattachment.models.generic.Resource
import com.example.myattachment.storage.data.PrefrenceManager
import com.example.myattachment.utils.NetworkUtils
import com.example.myattachment.utils.RequestService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthRepository (application: Application) {
    val signInObservable = MutableLiveData<Resource<SignInRes>>()
    private val context: Context = application.applicationContext

    fun signIn (signIn: SignIn) : LiveData<Resource<SignInRes>> {
        if (NetworkUtils.isConnected(context)) {
            signInObservable.postValue(Resource.loading(null))
            GlobalScope.launch(context = Dispatchers.Main) {
                val call = RequestService.getService("").signIn(signIn)
                call.enqueue(object : Callback<SignInRes> {
                    override fun onFailure(call: Call<SignInRes>?, t: Throwable?) {
                        signInObservable.postValue(Resource.error(t.toString(), null))
                    }
                    override fun onResponse(call: Call<SignInRes>?, response: Response<SignInRes>?) {
                        if (response != null) {
                            if (response.isSuccessful) {
                                if (response.body()?.success !!) {
                                    if(response.body()?.status_code == 1){
                                        PrefrenceManager(context!!).setUserSession(response.body()?.data)
                                        response.body()?.let { signInObservable.postValue(Resource.success(it))  }
                                    }else{
                                        response.body()?.errors?.let {  signInObservable.postValue(Resource.error(response.body()?.message + "\n" + it.joinToString { "\n" }, null)) }
                                    }
                                } else {
                                    response.body()?.errors?.let { signInObservable.postValue(Resource.error( response.body()?.message + "\n" + it.joinToString { "\n" }, null)) }
                                }
                            } else {
                                signInObservable.postValue(Resource.error( response.toString(), null))
                            }
                        } else {
                            signInObservable.postValue(Resource.error("Error Logging In", null))
                        }
                    }
                })
            }
        } else {
            signInObservable.postValue(Resource.error(context.getString(R.string.no_connection), null))
        }
        return signInObservable
    }
}