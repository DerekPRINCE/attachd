package com.example.myattachment.ui.auth

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.myattachment.models.auth.SignIn
import com.example.myattachment.models.auth.SignInRes
import com.example.myattachment.models.generic.Resource
import com.example.myattachment.storage.auth.AuthRepository

class AuthViewModel  (application: Application) : AndroidViewModel(application) {
    private val signInObservable = MediatorLiveData<Resource<SignInRes>>()
    var authRepository: AuthRepository = AuthRepository(application)
    init {
        signInObservable.addSource(authRepository.signInObservable) { data -> signInObservable.setValue(data) }
    }

    fun observeSignIn(): LiveData<Resource<SignInRes>> {
        return signInObservable
    }

    fun signIn(signIn: SignIn){
        authRepository.signIn(signIn)
    }
}