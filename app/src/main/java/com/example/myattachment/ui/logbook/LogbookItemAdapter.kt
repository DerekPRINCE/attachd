package com.example.myattachment.ui.logbook

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myattachment.R
import com.example.myattachment.models.logbook.LogBookItem
import com.example.myattachment.utils.OnRecyclerViewItemClick


class LogbookItemAdapter (private var modelList: List<LogBookItem>?, private val recyclerListener: OnRecyclerViewItemClick) : RecyclerView.Adapter<LogbookItemViewHolder>() {
    var itemView: View? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LogbookItemViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.logbookitem_card, parent, false)
        return LogbookItemViewHolder(itemView!!, recyclerListener)
    }

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: LogbookItemViewHolder, position: Int) {
        val model = modelList!![position]
        holder.label.text = model.label
        holder.datetime.text = model.datetime
        holder.street.text = model.address?.street
    }

    override fun getItemCount(): Int {
        return if (null != modelList) modelList!!.size else 0
    }

    fun refresh(modelList: List<LogBookItem>) {
        this.modelList = modelList
        notifyDataSetChanged()
    }
}