package com.example.myattachment.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myattachment.R
import com.example.myattachment.models.address.Address
import com.example.myattachment.models.user.User
import com.example.myattachment.storage.data.PrefrenceManager
import com.example.myattachment.ui.home.LecturerHomeFragment
import com.example.myattachment.ui.home.StudentHomeFragment
import com.example.myattachment.ui.setting.SettingFragment
import com.example.myattachment.utils.Xit
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_home.*

class MainActivity : AppCompatActivity() {
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_home -> {
//                Xit.openFragment(this, StudentHomeFragment.newInstance(), null, R.id.llMiddle)
                showHome(PrefrenceManager(this).getUserType()!!)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_settings -> {
                Xit.openFragment(this, SettingFragment.newInstance(), "SettingFragment", R.id.llMiddle)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        PrefrenceManager(this).setUserType(1)
        if (savedInstanceState == null) {
            showHome(PrefrenceManager(this).getUserType()!!)
        }
        PrefrenceManager(this).setLoginStatus(1)
        PrefrenceManager(this).setUserSession(
            User(
                id=0,
                email = "derekmwirigi@gmail.com",
                firebaseToken = "",
                userName = "Derek",
                password = "",
                image = "",
                token = ""
            )
        )
        PrefrenceManager(this).setAddress(
            Address(0,
                "Westlands",
                "1.0",
                "38.0",
                ""
            )
        )
    }

    override fun onResume() {
        super.onResume()
        showHome(PrefrenceManager(this).getUserType()!!)
    }

    private fun showHome(userType: Int){
        if(userType == 1) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.llMiddle, StudentHomeFragment.newInstance())
                .commitNow()
        }else{

            supportFragmentManager.beginTransaction()
                .replace(R.id.llMiddle, LecturerHomeFragment.newInstance())
                .commitNow()
        }
    }
}
