package com.example.myattachment.ui.logbook

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myattachment.R
import com.example.myattachment.utils.OnRecyclerViewItemClick
import java.lang.ref.WeakReference


class LogbookItemViewHolder (itemView: View, listener: OnRecyclerViewItemClick) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
    View.OnLongClickListener {
    private val listenerWeakReference: WeakReference<OnRecyclerViewItemClick> = WeakReference(listener)
    var itemVew: View = itemView
    var label: TextView = itemView.findViewById(R.id.tvLabel)
    var datetime: TextView = itemView.findViewById(R.id.tvDateTime)
    var street: TextView = itemView.findViewById(R.id.tvLocation)
    init {
        itemView.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
    }
    override fun onLongClick(p0: View?): Boolean {
        listenerWeakReference.get()?.onLongClickListener(adapterPosition)
        return true
    }
    override fun onClick(v: View?) {
        listenerWeakReference.get()?.onClickListener(adapterPosition)
    }
}