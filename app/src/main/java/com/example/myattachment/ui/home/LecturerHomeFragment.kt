package com.example.myattachment.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myattachment.R
import com.example.myattachment.models.student.Student
import com.example.myattachment.models.user.User
import com.example.myattachment.storage.data.PrefrenceManager
import com.example.myattachment.ui.student.StudentAdapter
import com.example.myattachment.utils.OnRecyclerViewItemClick
import kotlinx.android.synthetic.main.fragment_home.*

class LecturerHomeFragment : Fragment() {
    companion object {
        fun newInstance() = LecturerHomeFragment()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    @SuppressLint("WrongConstant")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mcvInfo.setOnClickListener {  }
        setUpUI()
    }

    private fun setUpUI (){
        var user: User = PrefrenceManager(activity!!).getUserSession()!!
        tvUserName.text = user.userName
        tvCredentials.text = user.email
        lecturerHome()
    }

    private fun lecturerHome() {
        // load assigned students
        var students = listOf(
            Student(1, "Derek Mugambi", "0706828557", "derek@code.me", "Maths and Comp", PrefrenceManager(this.activity!!).getAddress(), "visited", "")
        )
        rvHomeGeneric.adapter = StudentAdapter(students, object : OnRecyclerViewItemClick {
            override fun onClickListener(position: Int) {
                Toast.makeText(activity!!, "" + position, Toast.LENGTH_LONG).show()
            }
            override fun onLongClickListener(position: Int) {
            }
        })
        rvHomeGeneric.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        rvHomeGeneric.itemAnimator = DefaultItemAnimator()
            tvHomePageLabel.text = "Lecturer Account"
            tvTitle.text = "Students"
                    fabAddLogbookItem.isVisible = false


    }
}