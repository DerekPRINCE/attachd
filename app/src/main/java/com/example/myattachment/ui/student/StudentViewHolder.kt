package com.example.myattachment.ui.student

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myattachment.R
import com.example.myattachment.utils.OnRecyclerViewItemClick
import java.lang.ref.WeakReference


class StudentViewHolder (itemView: View, listener: OnRecyclerViewItemClick) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
    View.OnLongClickListener {
    private val listenerWeakReference: WeakReference<OnRecyclerViewItemClick> = WeakReference(listener)
    var itemVew: View = itemView
    var names: TextView = itemView.findViewById(R.id.tvNames)
    var course: TextView = itemView.findViewById(R.id.tvCourse)
    var address: TextView = itemView.findViewById(R.id.tvAddress)
    var visitedStatus: TextView = itemView.findViewById(R.id.tvVisitedStatus)
    init {
        itemView.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
    }
    override fun onLongClick(p0: View?): Boolean {
        listenerWeakReference.get()?.onLongClickListener(adapterPosition)
        return true
    }
    override fun onClick(v: View?) {
        listenerWeakReference.get()?.onClickListener(adapterPosition)
    }
}