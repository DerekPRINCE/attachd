package com.example.myattachment.ui.logbook

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.myattachment.R
import com.example.myattachment.models.logbook.LogBookItem
import com.example.myattachment.storage.data.PrefrenceManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.logbookitem_create.*
import java.util.*

class LogbookAddItemFragment  : Fragment() {
    companion object {
        fun newInstance() = LogbookAddItemFragment()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.logbookitem_create, container, false)
    }

    @SuppressLint("WrongConstant")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupUI()
    }

    private fun setupUI(){
        btAdd.setOnClickListener{
            Toast.makeText(this.activity!!, Gson().toJson(getDataFromUI()), Toast.LENGTH_LONG).show()
        }
    }

    private fun getDataFromUI(): LogBookItem{
        return LogBookItem(
            id=0,
            label = edtLabel.text.toString(),
            content = edtContent.text.toString(),
            datetime = edtDate.text.toString(),
            description = "Attachment activity on " + Date().time,
            address = PrefrenceManager(activity!!).getAddress())
    }
}