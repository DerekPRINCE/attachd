package com.example.myattachment.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myattachment.R
import com.example.myattachment.models.address.Address
import com.example.myattachment.models.logbook.LogBookItem
import com.example.myattachment.models.user.User
import com.example.myattachment.storage.data.PrefrenceManager
import com.example.myattachment.ui.logbook.LogbookAddItemFragment
import com.example.myattachment.ui.logbook.LogbookItemAdapter
import com.example.myattachment.utils.OnRecyclerViewItemClick
import com.example.myattachment.utils.Xit
import kotlinx.android.synthetic.main.fragment_home.*

class StudentHomeFragment : Fragment() {
    companion object {
        fun newInstance() = StudentHomeFragment()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    @SuppressLint("WrongConstant")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mcvInfo.setOnClickListener {  }
        setUpUI()
    }

    private fun setUpUI (){
        var user: User = PrefrenceManager(activity!!).getUserSession()!!
        tvUserName.text = user.userName
        tvCredentials.text = user.email
        studentHome()
    }

    private fun studentHome() {
        // load log book items
        var logbookItems = listOf(
            LogBookItem(0, "Network", "Added stuff", "Stuff stuff", "15, March 2020", Address(0, "Westlands, Kenya", "-1.002", "36.009", "Nice")),
            LogBookItem(0, "Network", "Added stuff", "Stuff stuff", "15, March 2020", Address(0, "Westlands, Kenya", "-1.002", "36.009", "Nice")),
            LogBookItem(0, "Network", "Added stuff", "Stuff stuff", "15, March 2020", Address(0, "Westlands, Kenya", "-1.002", "36.009", "Nice")),
            LogBookItem(0, "Network", "Added stuff", "Stuff stuff", "15, March 2020", Address(0, "Westlands, Kenya", "-1.002", "36.009", "Nice")),
            LogBookItem(0, "Network", "Added stuff", "Stuff stuff", "15, March 2020", Address(0, "Westlands, Kenya", "-1.002", "36.009", "Nice")),
            LogBookItem(0, "Network", "Added stuff", "Stuff stuff", "15, March 2020", Address(0, "Westlands, Kenya", "-1.002", "36.009", "Nice")),
            LogBookItem(0, "Network", "Added stuff", "Stuff stuff", "15, March 2020", Address(0, "Westlands, Kenya", "-1.002", "36.009", "Nice")),
            LogBookItem(0, "Network", "Added stuff", "Stuff stuff", "15, March 2020", Address(0, "Westlands, Kenya", "-1.002", "36.009", "Nice")),
            LogBookItem(0, "Network", "Added stuff", "Stuff stuff", "15, March 2020", Address(0, "Westlands, Kenya", "-1.002", "36.009", "Nice")),
            LogBookItem(0, "Network", "Added stuff", "Stuff stuff", "15, March 2020", Address(0, "Westlands, Kenya", "-1.002", "36.009", "Nice"))
        )
        rvHomeGeneric.adapter = LogbookItemAdapter(logbookItems, object : OnRecyclerViewItemClick {
            override fun onClickListener(position: Int) {
                Toast.makeText(activity!!, "" + position, Toast.LENGTH_LONG).show()
            }
            override fun onLongClickListener(position: Int) {
            }
        })
        rvHomeGeneric.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        rvHomeGeneric.itemAnimator = DefaultItemAnimator()

        fabAddLogbookItem.setOnClickListener{
            Xit.openFragment(this.activity!!, LogbookAddItemFragment.newInstance(), "LogbookAddItemFragment", R.id.llMiddle)
        }
        tvHomePageLabel.text = "Student Account"
        tvTitle.text = "Log Book"
        fabAddLogbookItem.isVisible = true
    }
}