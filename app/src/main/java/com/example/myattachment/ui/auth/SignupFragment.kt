package com.example.myattachment.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myattachment.R
import com.example.myattachment.models.auth.SignUp
import com.example.myattachment.storage.data.PrefrenceManager
import com.example.myattachment.ui.MainActivity
import com.example.myattachment.utils.Xit
import kotlinx.android.synthetic.main.signup_layout.*

class SignupFragment : Fragment() {
    private lateinit var viewModel: AuthViewModel
    companion object {
        fun newInstance() = SignupFragment()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.signup_layout, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AuthViewModel::class.java)

    }

    private fun verifyData () : SignUp {
        return SignUp(edtUserName.text.toString(), edtEmail.text.toString(), edtPassword.text.toString())
    }
}