package com.example.myattachment.ui.student

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myattachment.R
import com.example.myattachment.models.student.Student
import com.example.myattachment.utils.OnRecyclerViewItemClick


class StudentAdapter (private var modelList: List<Student>?, private val recyclerListener: OnRecyclerViewItemClick) : RecyclerView.Adapter<StudentViewHolder>() {
    var itemView: View? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        itemView = LayoutInflater.from(parent.context).inflate(R.layout.student_card, parent, false)
        return StudentViewHolder(itemView!!, recyclerListener)
    }

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        val model = modelList!![position]
        holder.names.text = model.names
        holder.course.text = model.course
        holder.address.text = model.address?.street
        holder.visitedStatus.text = model.visited
    }

    override fun getItemCount(): Int {
        return if (null != modelList) modelList!!.size else 0
    }

    fun refresh(modelList: List<Student>) {
        this.modelList = modelList
        notifyDataSetChanged()
    }
}