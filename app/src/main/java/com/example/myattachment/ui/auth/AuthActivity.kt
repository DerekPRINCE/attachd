package com.example.myattachment.ui.auth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myattachment.R
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.flContents, SigninFragment.newInstance())
                .commitNow()
        }
        btSignIn.setOnClickListener { showFragment("signIn") }
    }
    private fun showFragment(fragment: String){
        when(fragment){
            "signIn"->{
                supportFragmentManager.beginTransaction()
                    .replace(R.id.flContents, SigninFragment.newInstance())
                    .commitNow()
            }
        }
    }
}
