package com.example.myattachment.utils

interface OnRecyclerViewItemClick {
    fun onClickListener(position: Int)
    fun onLongClickListener(position: Int)
}