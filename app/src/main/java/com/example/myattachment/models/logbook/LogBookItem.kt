package com.example.myattachment.models.logbook

import com.example.myattachment.models.address.Address
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LogBookItem {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("label")
    @Expose
    var label: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("content")
    @Expose
    var content: String? = null

    @SerializedName("datetime")
    @Expose
    var datetime: String? = null

    @SerializedName("address")
    @Expose
    var address: Address? = null


    constructor(id: Int?, label: String?, description: String?, content: String?, datetime: String?, address: Address?) {
        this.id = id
        this.label = label
        this.description = description
        this.content = content
        this.datetime = datetime
        this.address = address
    }
}