package com.example.myattachment.models.address

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Address {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("street")
    @Expose
    var street: String? = null

    @SerializedName("lat")
    @Expose
    var lat: String? = null

    @SerializedName("lng")
    @Expose
    var lng: String? = null

    @SerializedName("info")
    @Expose
    var info: String? = null


    constructor(id: Int?, street: String?, lat: String?, lng: String?, info: String?) {
        this.id = id
        this.street = street
        this.lat = lat
        this.lng = lng
        this.info = info
    }
}