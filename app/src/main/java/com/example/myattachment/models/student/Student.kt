package com.example.myattachment.models.student

import androidx.room.Entity
import androidx.room.Index
import com.example.myattachment.models.address.Address
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(
    indices = [(Index("id"))],
    primaryKeys = ["id"]
)
class Student(
    @field:SerializedName("id")
    @Expose
    var id: Int?,

    @field:SerializedName("names")
    @Expose
    var names: String?,

    @field:SerializedName("phone")
    @Expose
    var phone: String?,


    @field:SerializedName("email")
    @Expose
    var email: String?,

    @field:SerializedName("course")
    @Expose
    var course: String?,

    @field:SerializedName("address")
    @Expose
    var address: Address?,

    @field:SerializedName("visited")
    @Expose
    var visited: String?,

    @field:SerializedName("image")
    @Expose
    var image: String?

)