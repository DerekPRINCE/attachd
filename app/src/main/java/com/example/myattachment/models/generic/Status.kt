package com.example.myattachment.models.generic

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}